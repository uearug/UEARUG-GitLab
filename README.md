# Welcome to the University of East Anglia R-Users Group GitLab account!

We created this account to encourage the people to support each other in using R 
for our daily work. This is a space for beginning, intermediate, and advanced 
users to learn from each other.

## Join our mailing list: 
If you are interested in joining our mailing list please send a request to 
<uea.rug@gmail.com>.

